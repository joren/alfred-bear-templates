## Installation

1. Add the workflow to Alfred.
2. Make sure to update the location of the templates

## Development

Update the code directly into Alfred, or work on them in this repo and then copy it to Alfred.

### Variables

Extend the list of variables and their content that can be replaced inside the each template and title.
