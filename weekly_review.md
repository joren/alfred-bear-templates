{{current_year}} week {{current_week_number}}
---
#weekly-review/{{current_year}}/{{current_month}}
---
::[Week {{last_week_number}}](bear://x-callback-url/open-note?title={{last_week_year}}%20week%20{{last_week_number}}):: - ::[Week {{next_week_number}}](bear://x-callback-url/open-note?title={{next_week_year}}%20week%20{{next_week_number}})::
---
## Terugkijken
- Agenda en notities nalopen
- Inboxen verwerken ::[Things Inbox](things:///show?id=inbox)::
- Projecten nalopen [- Jira](https://getsilverfin.atlassian.net/projects/GREEN?selectedItem=com.atlassian.plugins.atlassian-connect-plugin%3Acom.herocoders.plugins.jira.epicsmap__epics-map-page)
- Downloads folder opkuisen
- Prioriteiten sinds ::[laatste review](bear://x-callback-url/open-note?title={{last_week_year}}%20week%20{{last_week_number}}):: bekijken

## Vooruitblikken
- Agenda plannen
- Prioriteiten voor komende week

---
## Meetings volgende week
* 

## Notes
### Wekelijkse prioriteiten
1. 

