import os, json, re
from os.path import expanduser, join, splitext

basepath = join(expanduser("~"), "code/personal/bear-templates")
files = os.listdir(basepath)

filenames = []

for file in files:
    match = re.match(".+\.md$", file)
    if match:
        filenames.append({
            'title': splitext(file)[0],
            'arg': join(basepath, file)
        })

print(json.dumps({'items': filenames}))
