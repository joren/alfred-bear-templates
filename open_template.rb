require "erb"
require "date"

args = ARGV.first
template_name, extra = args.split(',')

template = File.read(template_name)
first_newline = (template.index("\n") || template.size - 1) + 1
title = template.slice!(0, first_newline).sub("\n",'')

def date_of_next(day)
  date  = Date.parse(day)
  delta = date > Date.today ? 0 : 7
  date + delta
end

{
  current_year: Date.today.year,
  current_month: Date.today.strftime("%B"),
  current_week_number: Date.today.cweek,
  last_week_year: (Date.today - 7).year,
  next_week_year: (Date.today + 7).year,
  last_week_number: (Date.today - 7).cweek,
  next_week_number: (Date.today + 7).cweek,
  year_month_day: Date.today.strftime("%Y%m%d"),
  next_tuesday: date_of_next("Tuesday").strftime("%d/%m/%Y"),
  extra_sprint: extra,
  extra_prev_sprint: extra.to_i - 1,
  extra_next_sprint: extra.to_i + 1,
  extra_who: extra,
  extra_meeting: extra,
  name: ARGV[1]
}.each do |tag, value|
  template.gsub!("{{#{tag}}}", value.to_s)
  title.gsub!("{{#{tag}}}", value.to_s)
end

p title
p template

uri = "bear://x-callback-url/create?title=#{ERB::Util.url_encode(title)}&open_note=yes&new_window=no&show_window=yes&pin=no&edit=yes&text=#{ERB::Util.url_encode(template)}"

p uri
%x[open "#{uri}"]
