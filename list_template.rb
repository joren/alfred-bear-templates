require "json"

args = ARGV[0].split(' ')
query = args[0]
extra = args[1]

# p args

base_dir = "/Users/joren/code/personal/bear-templates/"
templates = Dir.new(base_dir).children.each_with_object([]) do |child, files|
  next unless child.match(/.+\.md$/)
  file_name = child.sub("\.md", '').split("_").map {|word| word.capitalize}.join(" ")
  if query
    if file_name.downcase.match?(query.downcase)
      files << {title: file_name, arg: "#{File.join(base_dir, child)},#{extra}"}
    end
  else
    files << {title: file_name, arg: File.join(base_dir, child)}
  end
end

result = {"items": templates}

puts result.to_json
